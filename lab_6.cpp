#include <iostream>
#include <fstream>
#include <unordered_map>
using namespace std;

int main() {
	fstream input("file.txt");
	long long int val;
	unordered_map<long long int, long long int>myrecipe;
	while (input >> val) {
		myrecipe.emplace(val, val);
	}
	cout << val << endl;
	cout << myrecipe.size() << endl;
	int count = 0;
	
	for (int s = -1000; s <= 1000; s++){
		for (unordered_map<long long int, long long int>::iterator it = myrecipe.begin(); it != myrecipe.end(); ++it){
			long long int x = it->first;
			auto search = myrecipe.find(s - x);
			if (search != myrecipe.end()) {
				cout << "Found " << search->first << " " << it->first << " " << (search->first - it->first) << endl;
				count++;
				break;
			}

		}
		cout << " s= " << s << endl;
	}
	
	cout <<"end"<< endl;
	cout << count << endl;
	return 0;
}