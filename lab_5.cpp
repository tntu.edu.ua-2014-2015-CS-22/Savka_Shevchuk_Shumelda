#include <string>
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;
void PrintVector(vector<int>& values)
{
	int i;

	for (i = 0; i != values.size(); i++)
	{
		cout << values[i] << endl;
	}
}

void Swap(vector<int>& values, const int& n1, const int& n2)
{
	int nTmp = values[n1];
	values[n1] = values[n2];
	values[n2] = nTmp;
}

int Parent(const int& position)
{
	return ((position + 1) / 2) - 1;
}

int Left(const int& position)
{
	return ((position + 1) * 2) - 1;
}

int Right(const int& position)
{
	return ((2 * (position + 1)) + 1) - 1;
}

void Heapify(vector<int>& heap, const int& nIndex, const int& nHeapSize)
{
	int nLeft = Left(nIndex);
	int nRight = Right(nIndex);
	int nLargest = -1;

	if (nLeft < nHeapSize && heap[nLeft] > heap[nIndex])
		nLargest = nLeft;
	else
		nLargest = nIndex;
	if (nRight < nHeapSize && heap[nRight] > heap[nLargest])
		nLargest = nRight;
	if (nLargest != nIndex)
	{
		Swap(heap, nIndex, nLargest);
		Heapify(heap, nLargest, nHeapSize);
	}

}

void BuildHeap(vector<int>& heap)
{
	int nHeapSize = heap.size();

	for (int i = (nHeapSize / 2) - 1; i > -1; i--)
	{
		Heapify(heap, i, nHeapSize);
	}
}

void HeapSort(vector<int>& values)
{
	int nHeapSize = values.size();

	BuildHeap(values);

	for (int i = values.size() - 1; i > 0; i--)
	{
		Swap(values, 0, i);
		nHeapSize = nHeapSize - 1;
		Heapify(values, 0, nHeapSize);
	}
}
void printmedian(vector<int>& values,int i)
{
	int size = values.size();
	ofstream output;
	output.open("output.txt", std::ofstream::out | std::ofstream::app);
	output << "iteration: ";
	output << i;
	output << endl;

	if (size % 2 == 0)
	{
		output << values[size / 2 - 2];
		output << endl;
		output << values[size / 2 - 1];
		output << endl;
	}
	else
	{
		output << values[size / 2];
		output << endl;
	}
	output.close();
}


int main(int argc, const char * argv[]) {
	ofstream output;
	output.open("output.txt", std::ofstream::out | std::ofstream::trunc);
	output.clear();
	output.close();
	vector<int> values;
	int d;
	ifstream input("filenum.txt");
	int i = 1;
	while (input >> d) {
		values.push_back(d);
		if (i == 2015) {
			HeapSort(values);
			printmedian(values,i);
		}
		if (i == 9876) {
			HeapSort(values);
			printmedian(values, i);
		}
		cout << i << endl;
		i++;
	}
	values.clear();
	return 0;
}