#include<stdio.h>
#include<stdlib.h>
#include<limits.h>
#include<algorithm>
#include<vector>
#include<string.h>
#include <fstream>
using namespace std;

struct heap_node
{
	int vertex_no;
	int vertex_value;
};

bool compare(struct heap_node a, struct heap_node b)
{
	if (a.vertex_value > b.vertex_value)
		return true;
	return false;
}

class node
{
public:
	int to, weight;
	node(int to, int weight);
};

node::node(int to, int weight)
{
	this->to = to;
	this->weight = weight;
}

class Graph
{
	int V, E;
	vector<node> *adj;
public:
	Graph(int V, int E);
	void addEdge(int v, int w, int weight);
	void dijkstra();
};

Graph::Graph(int V, int E)
{
	this->V = V;
	this->E = E;
	adj = new vector<node>[V];
}

void Graph::addEdge(int v, int w, int weight)
{
	node x(w, weight);
	adj[v].push_back(x);
	node y(v, weight);
	adj[w].push_back(y);
}

void Graph::dijkstra()
{
	int src = 100562;
	const int V = 1070345;
	struct heap_node nodex[V];
	for (int i = 0; i<V; i++)
	{
		nodex[i].vertex_value = INT_MAX;
		nodex[i].vertex_no = i;
	}
	bool visited[V];
	memset(visited, false, sizeof(visited));
	nodex[src].vertex_value = 0;
	make_heap(nodex, nodex + V, compare);
	int val = 0;
	visited[src] = true;
	for (int i = 0; i<V - 1; i++)
	{
		pop_heap(nodex, nodex + V - i, compare);
		int cur = V - i - 1;
		int u = nodex[cur].vertex_no;
		vector<node>::iterator it;
		visited[u] = true;
		for (it = adj[u].begin(); it != adj[u].end(); it++)
		{
			node v = *it;
			for (int j = 0; j<V; j++)
			{
				if (nodex[j].vertex_no == v.to)
				{
					val = j;
					break;
				}
			}
			if (!visited[v.to] && nodex[cur].vertex_value != INT_MAX && nodex[val].vertex_value > nodex[cur].vertex_value + v.weight)
			{
				nodex[val].vertex_value = nodex[cur].vertex_value + v.weight;
			}
		}
		make_heap(nodex, nodex + V - i - 1, compare);
	}
	printf("The shortest distance from %d\n", src);
	for (int i = 0; i<V; i++)
	{
		printf("node %d : %d\n", nodex[i].vertex_no, nodex[i].vertex_value);
	}
}

int main()
{
	int V = 1070376;
	int E = 2712798;
	Graph g(V, E);
	fstream input("file.txt");
	int v, w, weight;
	while (input >> v >> w >> weight) {
		g.addEdge(v, w, weight);
	}
	g.dijkstra();
	return 0;
}