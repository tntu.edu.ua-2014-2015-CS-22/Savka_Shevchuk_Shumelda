#include <iostream>
#include <list>
#include <stack>
#include <fstream>
using namespace std;

class Graph
{
	unsigned int V;
	list<unsigned int> *adj;

	void fillOrder(unsigned int v, bool visited[], stack<unsigned int> &Stack);

	void DFSUtil(unsigned int v, bool visited[]);
public:
	Graph(unsigned int V);
	void addEdge(unsigned int v, unsigned int w);

	void printSCCs();

	Graph getTranspose();
};

Graph::Graph(unsigned int V)
{
	this->V = V;
	adj = new list<unsigned int>[V];
}

void Graph::DFSUtil(unsigned int v, bool visited[])
{
	visited[v] = true;
	cout << v << " ";

	list<unsigned int>::iterator i;
	for (i = adj[v].begin(); i != adj[v].end(); ++i)
	if (!visited[*i])
		DFSUtil(*i, visited);
}

Graph Graph::getTranspose()
{
	Graph g(V);
	for (unsigned int v = 0; v < V; v++)
	{
		list<unsigned int>::iterator i;
		for (i = adj[v].begin(); i != adj[v].end(); ++i)
		{
			g.adj[*i].push_back(v);
		}
	}
	return g;
}

void Graph::addEdge(unsigned int v, unsigned int w)
{
	adj[v].push_back(w); 
}

void Graph::fillOrder(unsigned int v, bool visited[], stack<unsigned int> &Stack)
{
	visited[v] = true;

	list<unsigned int>::iterator i;
	for (i = adj[v].begin(); i != adj[v].end(); ++i)
	if (!visited[*i])
		fillOrder(*i, visited, Stack);

	Stack.push(v);
}

void Graph::printSCCs()
{
	stack<unsigned int> Stack;

	bool *visited = new bool[V];
	for (int i = 0; i < V; i++)
		visited[i] = false;

	for (unsigned int i = 0; i < V; i++)
	if (visited[i] == false)
		fillOrder(i, visited, Stack);

	Graph gr = getTranspose();

	for (unsigned int i = 0; i < V; i++)
		visited[i] = false;

	while (Stack.empty() == false)
	{
		unsigned int v = Stack.top();
		Stack.pop();

		if (visited[v] == false)
		{
			gr.DFSUtil(v, visited);
			cout << endl;
		}
	}
}

int main()
{
	unsigned int edge1, edge2;
	Graph g(875714);
	fstream input("file.txt");
	while (input >> edge1 >> edge2 ) {
		//cout << edge1 << " " << edge2 << endl;
		g.addEdge(edge1, edge2);
	}
	cout << "strongly connected components  \n";
	g.printSCCs();

	return 0;
}